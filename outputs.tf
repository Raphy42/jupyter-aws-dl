output "informations" {
  value = <<EOF
recap:
  - instance type: ${var.instance_type}
  - provision script: ${var.provision_script}

EC2 instance: ${var.name}
  IP: ${aws_instance.dl_instance.public_ip}
  DNS: ${aws_instance.dl_instance.public_dns}

EOF
}

output "instance_public_ip" {
  value = "${aws_instance.dl_instance.public_dns}"
}

output "ssh_public_key" {
  value = "${tls_private_key.ssh_key.public_key_openssh}"
}