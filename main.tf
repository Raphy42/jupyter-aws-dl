resource "tls_private_key" "ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = "safespace"
  public_key = "${tls_private_key.ssh_key.public_key_openssh}"
}

resource "aws_security_group" "jupyter" {
  tags = {
    Name = "jupyter-sg"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "port888" {
  security_group_id = "${aws_security_group.jupyter.id}"
  type = "ingress"
  from_port = 0
  to_port = 8888
  protocol = "tcp"
  cidr_blocks = ["0.0.0.0/0"]
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = "${data.aws_vpc.default.id}"
}

resource "aws_instance" "dl_instance" {
  ami = "${var.aws_ami}"
  instance_type = "${var.instance_type}"
  vpc_security_group_ids = ["${aws_security_group.jupyter.id}"]
  associate_public_ip_address = true

  key_name = "${aws_key_pair.generated_key.key_name}"

  root_block_device {
    volume_size = 500
    volume_type = "gp2"
    delete_on_termination = false
  }

  tags {
    Name = "${var.name}"
  }

  provisioner "remote-exec" {
    script = "${var.provision_script}"
  }
}