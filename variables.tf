# REQUIRED
variable "provision_script" {
  type = "string"
  description = "Script used to provision the instance (set provision_script=/path/to/script in terraform.tfvars to skip this step)"
}

variable "name" {
  type = "string"
  description = "Name of the instance"
}

variable "aws_access_key" {
  type = "string"
  description = "AWS access key"
}

variable "aws_secret_key" {
  type = "string"
  description = "AWS secret key"
}

# DEFAULTS
variable "instance_type" {
  type = "string"
  description = "Instance type"
  default = "p2.xlarge"
}

variable "aws_ami" {
  type = "string"
  description = <<DESC
Deep Learning AMI
see https://us-west-2.console.aws.amazon.com/ec2/v2/home?region=us-west-2#Images:visibility=public-images;search=Deep%20learning;sort=name
DESC
  default = "ami-01a4e5be5f289dd12"
}