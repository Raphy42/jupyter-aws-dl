#!/bin/sh -x

set -e

export CONDA_VERSION=4.5.0

# Go to home directory
cd ~

wget https://repo.continuum.io/archive/Anaconda3-${CONDA_VERSION}-Linux-x86_64.sh
bash Anaconda3-${CONDA_VERSION}-Linux-x86_64.sh -b -p ~/anaconda
rm Anaconda3-${CONDA_VERSION}-Linux-x86_64.sh
echo 'export PATH="~/anaconda/bin:$PATH"' >> ~/.bashrc

source ~/.bashrc

conda update conda